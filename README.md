**README**

SeedMe java client provides a convenient set of methods to interact with the SeedMe web services. This client provides programmatic access to perform certain tasks in the web service.

- - -

**Requirements**

-An account at SeedMe.org

-Java 1.5 or higher

-Apache ant

- - -

**System and Terminology**

SeedMe is a platform to share data. [See more information here](https://www.seedme.org/help/intro)

**Collection** is a container for that may have following elements

- **collection_id:** Each collection is automatically assigned a unique numeric identifier
- **Metadata:** Title, Description, Key-Value Pairs, Credits, License for the collection
- **Files:** These could be files, images, videos
- **Sequences:** Set of related images 
- **Tickers:** Short text string (128 chars) useful for monitoring progress
- **File size:** Each file must be less than 100 MB 

- - -

**Build**

The client is distributed as a maven repository. You can find configuration samples for several package managers at http://mvnrepository.com/artifact/org.seedme/seedme-client/0.1.0

The sample app demonstrating the SeedMe Java client is provided [here](https://bitbucket.org/seedme/seedme-java-demo)

Refer to maven quick start guide for creating a new project and adding dependencies: https://maven.apache.org/guides/getting-started/index.html

Others package tools can also be used, like Apache Ant, Apache Ivy, etc.

- - -

**Contact**

Dmitry Mishin <dmishin@sdsc.edu>