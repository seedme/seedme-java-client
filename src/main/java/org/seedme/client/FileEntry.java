package org.seedme.client;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

public class FileEntry {

	public File file;
	public LinkedList<File> filepaths = new LinkedList<File>();

	public String title;
	public String description;
	public String poster;
	public float fps = 30;
	public boolean encode = true;
	public boolean transcode = true;
	public boolean overwrite;

	/**
	 * Creates a FileEntry object
	 */
	public FileEntry() {

	}

	/**
	 * Creates a FileEntry with a filepath
	 * 
	 * @param f File path
	 */
	public FileEntry(String f) {
		file = new File(f);
		filepaths.add(new File(f));
	}

	/**
	 * Create a FileEntry with a LinkedList
	 * 
	 * @param fp list of filepaths
	 */
	public FileEntry(List<String> fp) {
		for (String curr : fp) {
			File f = new File(curr);
			filepaths.add(f);
		}
	}

	/**
	 * Returns a list of filepaths
	 * 
	 * @return List of filepaths
	 */
	public List<File> getFilePaths() {
		return this.filepaths;
	}

	/**
	 * Sets the title
	 * 
	 * @param t FileEntry title
	 */
	public void setTitle(String t) {
		title = t;
	}

	/**
	 * Sets the description
	 * 
	 * @param d Description of FileEntry
	 */
	public void setDescription(String d) {
		description = d;
	}

	/**
	 * Sets the poster link
	 * 
	 * @param p Poster filepath
	 */
	public void setPoster(String p) {
		poster = p;
	}

	/**
	 * Sets the frame rate
	 * 
	 * @param f frame rate
	 */
	public void setFPS(float f) {
		fps = f;
	}

	/**
	 * Sets encode for FileEntry
	 * 
	 * @param e encode
	 */
	public void setEncode(boolean e) {
		encode = e;
	}

	/**
	 * Sets transcode for FileEntry
	 * 
	 * @param t transcode
	 */
	public void setTranscode(boolean t) {
		transcode = t;
	}

	/**
	 * Sets overwrite for each FileEntry
	 * 
	 * @param t overwrite
	 */
	public void setOverwrite(boolean t) {
		overwrite = t;
	}
}
