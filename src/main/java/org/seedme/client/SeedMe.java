package org.seedme.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.seedme.client.FileEntry;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class SeedMe {
	private static final Logger logger = LogManager.getLogger(SeedMe.class);

	private String baseurl = "https://www.seedme.org";
	public String getUsername() {
		return username;
	}

	public String getApikey() {
		return apikey;
	}

	public String getServicesUrl() {
		return baseurl + "/services/1.1/collection1_1";
	}

	public String getCollectionUrl(int collectionId) {
		return baseurl+"/node/"+collectionId;
	}
	
	private String service_version = "1.1";
	private String username = "";
	private String apikey = "";
	private String cid = "";
	private String success_cid = "No CID from server";
	private String server_message = "No response from server";
	private final long MAX_SIZE = 100 * 1024 * 1024;
	private final long MAX_FILES = 20;
	MultipartEntityBuilder httpEnt = MultipartEntityBuilder.create();
	
	/**
	 * Query the content on various selections
	 * 
	 * @param qid collection id
	 * @param keyvalues Hashmap of keyvalues
	 * @param content content to query
	 * @param tail number of queries
	 * @return Response from server
	 * @throws RequestException error from SeedMe
	 */
	public ArrayList<JSONObject> query(String qid,
			HashMap<String, String> keyvalues, String content, String tail) throws RequestException {

		String response = null;
		if (content != null
				&& (content.equals("all") || content.equals("keyvalue")
						|| content.equals("kv") || content.equals("tic")
						|| content.equals("ticker") || content.equals("url"))) {
			// Consider the contents
			if (content.equals("kv")) {
				content = "keyvalue";
			}
			if (content.equals("tic")) {
				content = "ticker";
			}
			// Query
			response = httpGetCall(qid, content, tail, null);
			return convertArr(response);

		} else if (content == null && keyvalues != null) {
			// Response from the server and analye
			response = httpGetCall(qid, content, tail, keyvalues);
			if (response != null) {
				return convertArr(response);
			}
		}
		return null;

	}

	/**
	 * Helper method to convert server response to array
	 * 
	 * @param response response from server
	 * @return Array of the server response
	 */
	private ArrayList<JSONObject> convertArr(String response) {
		ArrayList<JSONObject> jsonArr = new ArrayList<JSONObject>();
		try {
			JSONObject jsonObject = new JSONObject(response);
			if (true) {
				// Iterate the keys
				SortedMap<String, String> map = new TreeMap();
				if(!jsonObject.has("files"))
					logger.debug(response);
				JSONArray filesArray = jsonObject.getJSONArray("files");
				for(int i=0; i<filesArray.length(); i++) {
					try {
						JSONObject fileObj = filesArray.getJSONObject(i);
						if(fileObj.has("filename"))
							map.put(fileObj.getString("filename"), fileObj.getString("url"));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				for (Map.Entry<String, String> entry : map.entrySet()) {
					JSONObject insert = new JSONObject();
					insert.put(entry.getKey(), entry.getValue());
					jsonArr.add(insert);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return jsonArr;
	}

	/**
	 * Helper method to retry download
	 * 
	 * @param retry number of times to retry
	 * @param interval the waiting time
	 * @return the response after retrying
	 * @throws RequestException 
	 */
	private ArrayList<JSONObject> downloadRetry(int retry, int interval) throws RequestException {
		ArrayList<JSONObject> result = query(cid, null, "url", null);
		if (result.size() == 0 && retry > 0) {
			for (int i = 0; i < retry; i++) {
				// Retry wait time.
				try {
					Thread.sleep(interval * 1000);
				} catch (InterruptedException e) {

					e.printStackTrace();
				}
				// Call method
				result = query(cid, null, "url", null);

				if (result.size() > 0) {
					break;
				}
			}
		}

		return result;
	}

	/**
	 * Download content from server
	 * 
	 * @param cid id to download from
	 * @param content conten to download
	 * @param path saving path
	 * @param retry retry interval
	 * @param interval retry interval time
	 * @param overwrite overwrite multiple files
	 * @throws RequestException error from SeedMe
	 */
	public void download(String cid, String content, String path, int retry,
			int interval, boolean overwrite) throws RequestException {

		// do query, nothing to download, sleep for (interval time), and then
		// retry as int
		// Even if private stop
		// Even if one available then send back
		if (content == null) {
			return;
		}
		ArrayList<JSONObject> jsArr = query(cid, null, "url", null);

		try {
			if (jsArr != null && jsArr.size() >= 1) {
					downloadSave(cid, content, path, overwrite, jsArr);
			} else {
				downloadRetry(retry, interval);
				if (jsArr != null && jsArr.size() >= 1) {
					downloadSave(cid, content, path, overwrite, jsArr);
				}
			}
		} catch (JSONException | IOException e) {
			e.printStackTrace();
			throw new RequestException(e.getMessage());
		}

	}

	/**
	 * Save the items returned
	 * 
	 * @param cid id to download from
	 * @param content content to download
	 * @param path path to download to
	 * @param overwrite overwrite material
	 * @param jsArr Array to download
	 * @throws JSONException 
	 * @throws IOException 
	 */
	private void downloadSave(String cid, String content, String path,
			boolean overwrite, ArrayList<JSONObject> jsArr) throws JSONException, IOException {
		for (JSONObject o : jsArr) {

			if (o.has("error") == true)
				return;

			if (path == null)
				path = System.getProperty("user.home") + "/Downloads";
			// Iterate through the array and save
			Iterator<?> x = o.keys();
			while (x.hasNext()) {
				String key = (String) x.next();
				try {
					// Account for various cases
					if (!(key.equals("status") || key
							.equals("collection_url"))) {
						String filename = key;
						if (content.equals("video") && getFileType(key).equals("video")) {
							    Request.Get(o.get(key).toString()).
						    	addHeader("username", username).
						    	addHeader("apikey", apikey).
						    	execute().saveContent(new File(path + "/" + filename));
						} else if (content.equals("all")) {
						    Request.Get(o.get(key).toString()).
						    	addHeader("username", username).
						    	addHeader("apikey", apikey).
						    	execute().saveContent(new File(path + "/" + filename));
						} else if (content.charAt(0) == '*') {
							if (content.length() >= 1 && key.endsWith(content.substring(1))) {
								Request.Get(o.get(key).toString()).
						    	addHeader("username", username).
						    	addHeader("apikey", apikey).
						    	execute().saveContent(new File(path + "/" + filename));
							}
						}
	
					}
				} catch(Exception ex) {
					System.err.println("Error downloading "+key+": "+ex.getMessage());
				}
			}

		}

	}

	/**
	 * Make HHTP GET call
	 * 
	 * @param qid id to call
	 * @param listlist of items to call on
	 * @param tail maximum number of items to download
	 * @param keyvalues keyvalues to search
	 * @return Response from server
	 * @throws RequestException 
	 */
	private String httpGetCall(String qid, String list, String tail,
			HashMap<String, String> keyvalues) throws RequestException {
		String url = this.getServicesUrl();

		if (qid != null && !qid.equals("")) {
			url = url + "/" + qid;
		}
		
		URIBuilder builder;
		try {
			builder = new URIBuilder(url);
		} catch (URISyntaxException e1) {
			throw new RuntimeException("Error using the services URL");
		}

		// Check for URL search

		if (keyvalues != null && keyvalues.size() != 0) {
			for (String set : keyvalues.keySet()) {
				String key = set.toString();
				String value = keyvalues.get(key).toString();
				if (!key.isEmpty() && !value.isEmpty()) {
					String output = key + ":" + value;
					builder.addParameter("keyvalue", output);
				}
			}
		}
		if (list != null && !list.equals("")) {
			builder.addParameter("list", list);
		}

		if (tail != null && !tail.equals("")) {
			builder.addParameter("tail", tail);
		}

		try(CloseableHttpClient httpclient = HttpClientBuilder.create()
				.build()) {

			HttpGet request = new HttpGet();
			// Call the client
			request.setURI(builder.build());
			request.addHeader("username", username);
			request.addHeader("apikey", apikey);

			HttpResponse response = httpclient.execute(request);
			
			if(response.getStatusLine().getStatusCode() != 200) {
				throw new RequestException("Error making request to "+builder.build().toASCIIString()+": "+response.getStatusLine().toString()+" "+EntityUtils.toString(response.getEntity()));
			}
			
			return EntityUtils.toString(response.getEntity());
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
			throw new RequestException("Error making request: "+e.getMessage(), e);
		}
	}

	/*
	 * Helper method to create collection and return CID
	 */

	private String create_collection_helper(String username,
			String apikey, String title) throws RequestException {
		// URL to execute
		HttpPost httppost = new HttpPost(this.getServicesUrl());

		MultipartEntityBuilder entb = MultipartEntityBuilder.create();
				
		if (title != null)
			entb.addPart("title", new StringBody(title,
					ContentType.TEXT_PLAIN));

		
		// Send in the username and api key
		HttpEntity reqEntity = 
				entb.build();

		httppost.setEntity(reqEntity);
		// Listen to the JSON response
		httppost.setHeader("username", username);
		httppost.setHeader("apikey", apikey);


		
		try(CloseableHttpClient httpclient = HttpClientBuilder.create()
				.build()) {
			HttpResponse response = httpclient.execute(httppost);
			if(response.getStatusLine().getStatusCode() != 200) {
				throw new RequestException("Error making request: "+response.getStatusLine().toString()+" "+EntityUtils.toString(response.getEntity()));
			}
			String responseString = EntityUtils.toString(response.getEntity());
			JSONObject jsonResponse = new JSONObject(responseString);
			String uploadedID = jsonResponse.getString("collection_id");
			
			return uploadedID;
		} catch (IOException e) {
			e.printStackTrace();
			throw new RequestException("Error making request: "+e.getMessage(), e);
		}
	}

	/**
	 * Calls update_collection method to add email account
	 * 
	 * @param cid Collection ID to update
	 * @param email email address to update
	 * @param notify notify the user or not.
	 * @return response from server
	 */
	public String add_email(int cid, String email, boolean notify) {

		return update_collection(cid, null, email, notify, null, null, null,
				null, false, null, null, null, null, null, null);
	}

	/**
	 * Calls update_collection method to add file to collectio
	 * 
	 * @param cid collection id
	 * @param filepath file path to the file
	 * @param title the file title
	 * @param description the file description
	 * @param overwrite the description of file
	 * @param poster the poster image for videos
	 * @param fps video only (default 30 seconds)
	 * @param encode encode video
	 * @param transcode transcode video
	 * @return Response from server
	 */
	public String add_file(int cid, String filepath, String title,
			String description, boolean overwrite, String poster, float fps,
			boolean encode, boolean transcode) {
		HashSet<FileEntry> file = new HashSet<FileEntry>();
		FileEntry temp = new FileEntry(filepath);
		file.add(temp);
		String response = update_collection(cid, null, null, false, title,
				description, null, null, overwrite, null, null, null, file,
				null, null);
		return response;
	}

	/**
	 * Adds keyvalue to the collection
	 * 
	 * @param cid collection id
	 * @param keyvalues HashMap; Key Value pairs for the collection
	 * @return Response from server
	 */
	public String add_keyvalue(int cid, Map<String, String> keyvalues) {
		return update_collection(cid, null, null, false, null, null, null,
				null, false, keyvalues, null, null, null, null, null);
	}

	/**
	 * Adds sequence to the server
	 * 
	 * @param cid collection id
	 * @param filepath path to the file
	 * @param title sequence title
	 * @param description sequence description
	 * @param fps frame rate for videos
	 * @param encode encode videos
	 * @param overwrite overwrite existing
	 * @return response from the server
	 */
	public String add_sequence(int cid, String filepath, String title,
			String description, float fps, boolean encode, boolean overwrite) {
		// Add a sequence to the temp set
		HashSet<FileEntry> sequences = new HashSet<FileEntry>();
		FileEntry temp = new FileEntry(filepath);
		temp.setTitle(title);
		temp.setDescription(description);
		temp.setFPS(fps);
		temp.setEncode(encode);
		temp.setOverwrite(overwrite);
		sequences.add(temp);
		String response = update_collection(cid, null, null, false, title,
				description, null, null, overwrite, null, null, null, null,
				null, null);

		return response;
	}

	/**
	 * Adds tag to the web collection
	 * 
	 * @param cid collection id
	 * @param tag strings to add
	 * @return response from the server
	 */
	public String add_tag(int cid, List<String> tag) {
		return update_collection(cid, null, null, false, null, null, null,
				null, false, null, tag, null, null, null, null);
	}

	/**
	 * Adds ticker to collection
	 * 
	 * @param cid collection id
	 * @param ticker tickers to add
	 * @return response from server
	 */
	public String add_ticker(int cid, List<String> ticker) {

		return update_collection(cid, null, null, false, null, null, null,
				null, false, null, null, ticker, null, null, null);

	}

	public void delete_collection(int col_id) throws ParseException, RequestException {
		URIBuilder builder;
		try {
			builder = new URIBuilder(getServicesUrl()+"/"+col_id);
		} catch (URISyntaxException e1) {
			throw new RuntimeException("Error using the services URL");
		}

		try(CloseableHttpClient httpclient = HttpClientBuilder.create()
				.build()) {

			HttpDelete request = new HttpDelete();
			request.setURI(builder.build());
			request.addHeader("username", username);
			request.addHeader("apikey", apikey);

			HttpResponse response = httpclient.execute(request);
			
			if(response.getStatusLine().getStatusCode() != 200) {
				throw new RequestException("Error making request to "+builder.build().toASCIIString()+": "+response.getStatusLine().toString()+" "+EntityUtils.toString(response.getEntity()));
			}
			
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
			throw new RequestException("Error making request: "+e.getMessage(), e);
		}
	}

	
	/**
	 * Creates collection with the given username and password
	 * 
	 * @param privacy privacy setting to access privacy
	 * @param sharing email to notify
	 * @param notify notify users
	 * @param title collection title
	 * @param description collection description
	 * @param credits Credits for the collection
	 * @param license license for the credits
	 * @param overwrite overwrite all files in collection
	 * @param keyvalues HashMap; keyvalues of collection
	 * @param tags tags for collection
	 * @param tickers ticker for collection
	 * @param files files to add to collection
	 * @param sequences set of sequences to add to collection
	 * @param transfer Transfer permissions to another user (by email)
	 * @return Response from server
	 * @throws RequestException Request to SeedMe failed
	 */
	public String create_collection(String privacy, String sharing,
			boolean notify, String title, String description, String credits,
			String license, boolean overwrite,
			HashMap<String, String> keyvalues, ArrayList<String> tags,
			ArrayList<String> tickers, HashSet<FileEntry> files,
			HashSet<FileEntry> sequences,
			String transfer) throws RequestException  {
		// Make sure user name isnt null
		String response = "Username or Apikey is not found";
		if (username != null && apikey != null) {
			// Create a collection if necessary
			cid = create_collection_helper(username, apikey, title);
			int i = -1;
			try {
				i = Integer.parseInt(cid);
			} catch (Exception nfe) {
				nfe.printStackTrace();
			}
			if (cid != null && i != -1) {
				response = update_collection(i, privacy, sharing, notify,
						title, description, credits, license, overwrite,
						keyvalues, tags, tickers, files, sequences,
						transfer);
			} else if (cid.equals("Site under maintenance")) {
				response = "SeedMe.org is under maintenance";
				this.server_message = response;
			} else if (cid.equals("No Response From Server")) {
				response = "No Response From Server";
				this.server_message = response;
			}
		}

		return response;
	}

	/*
	 * Make the restful call to the webservice
	 * 
	 * @param url
	 * 
	 * @param ent
	 * 
	 * @return Response from the server
	 */
	private String restPost(String url, MultipartEntityBuilder ent) {
		HttpPost httppost = new HttpPost(url);
		
		httppost.setHeader("username", username);
		httppost.setHeader("apikey", apikey);
		
		HttpClient httpclient = HttpClients.createDefault();
		String responseStr = null;
		// Make a http post call with entity
		httppost.setEntity(ent.build());

		try {

			HttpResponse response = httpclient.execute(httppost);
			responseStr = EntityUtils.toString(response.getEntity());

		} catch (IOException e) {

			e.printStackTrace();
		}

		// pureResponse(responseStr);
		return parseType(responseStr);
	}

	public String parseType(String response) {
		try {
			JSONObject jsonResponse = new JSONObject(response);
			String status = jsonResponse.getString("status");
			if (!(status.equals("success"))) {
				return this.getResponseMessage(jsonResponse);
			} else {
				return this.get_id(jsonResponse);
			}

		} catch (JSONException e) {

			e.printStackTrace();
		}
		return "No input received from client";
	}

	/**
	 * Update the collection with encoded sequence
	 * 
	 * @param cid Collection id
	 * @param title Sequence title
	 * @param fps Frame rate
	 * @return response from server
	 */
	public String encode_sequence(int cid, String title, float fps) {
		// Create temporary Hashset with the specified sequence
		HashSet<FileEntry> sequences = new HashSet<FileEntry>();
		FileEntry temp = new FileEntry();
		temp.setTitle(title);
		temp.setFPS(fps);
		sequences.add(temp);
		String response = update_collection(cid, null, null, false, null, null,
				null, null, false, null, null, null, null, null, null);
		return response;
	}

	/**
	 * Convert string to Json object and extract cid
	 * 
	 * @param result response from server
	 * @return CID
	 */
	public String extract_cid(String result) {
		String response = "No CID available";
		try {
			JSONObject obj = new JSONObject(result);
			response = obj.getString("collection_id");
		} catch (JSONException e) {

			e.printStackTrace();
		}
		return response;
	}

	/**
	 * Convert the String to JSON object and retrieve the status
	 * 
	 * @param result Response from server
	 * @return status from server
	 */
	public String extract_status(String result) {
		String response = "No status available";
		try {
			JSONObject obj = new JSONObject(result);
			response = obj.getString("status");
		} catch (JSONException e) {

			e.printStackTrace();
		}
		return response;
	}

	/**
	 * Update the collection with notification settings
	 * 
	 * @param cid collection id
	 * @param email email to notify
	 * @return Response from server
	 */
	public String notify(int cid, String email) {
		return update_collection(cid, null, email, true, null, null, null,
				null, false, null, null, null, null, null, null);
	}

	/**
	 * Retrieve info from the authorization file
	 * 
	 * @param filepath filepath to auth file
	 * @return Message from trying to retrieve username and apikey
	 */
	public String set_auth_via_file(String filepath) {

		File file = new File(filepath);
		String message = "Success";
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

			String login_info = reader.readLine();
			JSONObject key = new JSONObject(login_info);
			username = key.getString("username");
			apikey = key.getString("apikey");
		} catch (IOException e) {
			e.printStackTrace();
			message = "Can't read the login info file " + filepath;
		} catch (JSONException e) {
			e.printStackTrace();
			message = "No JSON item found in file";
		}

		return message;
	}

	/**
	 * Set the authorization via string
	 * 
	 * @param u username for SeedMe
	 * @param a authorization key for SeedMe
	 */
	public void set_auth_via_string(String u, String a) {
		if (u == null || u == "") {
		}
		username = u;
		apikey = a;
	}

	/**
	 * Set Base URL
	 * 
	 * @param url url
	 */
	public void set_base_url(String url) {
		baseurl = url;
	}

	/**
	 * Set sharing method
	 * 
	 * @param cid collection id
	 * @param email email to notify
	 * @param notify notify users
	 * 
	 */
	public void share(int cid, String email, boolean notify) {
		update_collection(cid, null, email, notify, null, null, null, null,
				false, null, null, null, null, null, null);
	}

	/**
	 * Returns the ID returned from server
	 * 
	 * @param response response from server
	 * @return Collection ID
	 */
	public String get_id(JSONObject response) {
		String responseString = "No Id found";

		try {
			if (response.getString("collection_id") != null) {
				responseString = response.getString("collection_id");
			}
		} catch (JSONException e) {

			e.printStackTrace();
		}
		this.success_cid = responseString;
		return this.success_cid;
	}

	/**
	 * Return the CID of successful collection
	 * 
	 * @return id
	 */
	public String get_id() {
		return this.success_cid;
	}

	/**
	 * Return the response from server
	 * 
	 * @return server message
	 */
	public String getResponseMessage() {
		return this.server_message;
	}

	private String getResponseMessage(JSONObject response) {
		String responseMsg = "No response from server";
		if (response != null) {
			try {
				if (response.has("error")) {
					responseMsg = response.getString("error");
				} else if (response.keySet().contains("warning")) {
					responseMsg = response.getString("warning");
				} else if (response.keySet().contains("sharing_error")) {
					responseMsg = response.getString("sharing_error");
				} else if (response.keySet().contains("file_error")) {
					responseMsg = response.getString("file_error");
				} else if (response.keySet().contains("plot_error")) {
					responseMsg = response.getString("plot_error");
				} else if (response.keySet().contains("sequence_error")) {
					responseMsg = response.getString("sequence_error");
				} else if (response.keySet().contains("vide_error")) {
					responseMsg = response.getString("video_error");
				} else if (response.keySet().contains("message")) {
					responseMsg = response.getString("message");
				} else if (response.keySet().contains("notify_message")) {
					responseMsg = response.getString("notify_message");
				} else if (response.keySet().contains("sequence_message")) {
					responseMsg = response.getString("sequence_message");
				}
			} catch (JSONException e) {

				e.printStackTrace();
			}
		}
		this.server_message = responseMsg;
		return responseMsg;
	}

	/**
	 * Update any content for an existing collection
	 * 
	 * @param cid collection id to SeedMe
	 * @param privacy privacy status of SeedMe
	 * @param sharing emails of people to share
	 * @param notify Send email to shared users
	 * @param title title for collection
	 * @param description description for collection
	 * @param credits credits for collection
	 * @param license license information for collection
	 * @param overwrite global overwrite
	 * @param keyvalues keyvalues for collection
	 * @param tags tags for the collection
	 * @param files List of files to upload
	 * @param sequences List of sequences to upload
	 * @param tickers List of tickers
	 * @param transfer Transfer permissions to another user (by email)
	 * @return Response from server
	 */
	public String update_collection(int cid, String privacy, String sharing,
			boolean notify, String title, String description, String credits,
			String license, boolean overwrite,
			Map<String, String> keyvalues, List<String> tags,
			List<String> tickers, Set<FileEntry> files,
			Set<FileEntry> sequences,
			String transfer) {
		String url = this.getServicesUrl() + "/" + cid + "/update";
		httpEnt = MultipartEntityBuilder.create();

		if (title != null)
			httpEnt.addPart("title", new StringBody(title,
					ContentType.TEXT_PLAIN));
		if (description != null)
			httpEnt.addPart("description", new StringBody(description,
					ContentType.TEXT_PLAIN));
		if (privacy != null)
			httpEnt.addPart("privacy", new StringBody(privacy,
					ContentType.TEXT_PLAIN));
		if (sharing != null)
			httpEnt.addPart("sharing", new StringBody(sharing,
					ContentType.TEXT_PLAIN));
		if (credits != null)
			httpEnt.addPart("credits", new StringBody(credits,
					ContentType.TEXT_PLAIN));
		if (license != null)
			httpEnt.addPart("license", new StringBody(license,
					ContentType.TEXT_PLAIN));
		if (transfer != null)
			httpEnt.addPart("transfer", new StringBody(transfer,
					ContentType.TEXT_PLAIN));

		httpEnt.addPart("overwrite", new StringBody(overwrite?"True":"False",
				ContentType.TEXT_PLAIN));
		httpEnt.addPart("notify", new StringBody(Boolean.toString(notify),
				ContentType.TEXT_PLAIN));
		int i = 0;
		// Set the keyvalues
		if (keyvalues != null) {
			for (Map.Entry<String, String> entry : keyvalues.entrySet()) {
				final String key = entry.getKey();
				if (!key.equals("title") && !key.equals("description")
						&& !key.equals("license") && !key.equals("credits")) {

					String value = entry.getValue();

					httpEnt.addPart("keyvalue[" + i + "]", new StringBody(
							(key + ":" + value), ContentType.TEXT_PLAIN));
					i++;
				}

			}
		}

		if (tags != null) {
			for (int z = 0; z < tags.size(); z++) {
				httpEnt.addPart("tag[" + z + "]",
						new StringBody(tags.get(z), ContentType.TEXT_PLAIN));
			}
		}

		if (tickers != null) {
			for (int z = 0; z < tickers.size(); z++) {
				httpEnt.addPart("ticker[" + z + "]",
						new StringBody(tickers.get(z),
								ContentType.TEXT_PLAIN));
			}
		}

		String response = "";

		if (files != null && !files.isEmpty()) {
			int i1 = 0;
			long size = 0;
			String identifier = "file";
			for (FileEntry entry : files) {
				String type = getFileType(entry.file.getName());
				if (type == null) {
					continue;
				}
				// Check for file length and number of files
				long length = entry.file.length();
				if (length + size >= MAX_SIZE || i1 == MAX_FILES - 1) {
					restPost(url, httpEnt);
					i1 = 0;
					size = 0;
					httpEnt = MultipartEntityBuilder.create();
				}

				// Set the file title, etc
				FileBody file = new FileBody(entry.file);
				httpEnt.addPart(identifier + "[" + i1 + "][filepath]", file);
				if (entry.title != null && !entry.title.isEmpty()) {
					httpEnt.addPart(identifier + "[" + i1 + "][title]",
							new StringBody(entry.title,
									ContentType.TEXT_PLAIN));
				}

				if (entry.description != null
						&& !entry.description.isEmpty()) {
					httpEnt.addPart(identifier + "[" + i1
							+ "][description]", new StringBody(
							entry.description, ContentType.TEXT_PLAIN));
				}

				String boolFileOvr = "False";

				if (entry.overwrite == true) {
					boolFileOvr = "True";
					httpEnt.addPart(identifier + "[" + i1 + "][overwrite]",
							new StringBody(boolFileOvr,
									ContentType.TEXT_PLAIN));
				}

				i1++;
				size += length;

			}
		}

		if (sequences != null && !sequences.isEmpty()) {

			int i1 = 0;
			int i2 = 0;
			long size = 0;
			for (FileEntry entry : sequences) {

				for (File next : entry.getFilePaths()) {
					String type = getFileType(next.getName());
					if (type == null) {
						continue;
					}

					long length = next.length();
					if (length + size >= MAX_SIZE || i1 == MAX_FILES - 1) {

						if (entry.title != null && !entry.title.isEmpty()) {
							httpEnt.addPart("sequence" + "[" + i2
									+ "][title]", new StringBody(
									entry.title, ContentType.TEXT_PLAIN));
						}

						if (entry.description != null
								&& !entry.description.isEmpty()) {
							httpEnt.addPart("sequence" + "[" + i2
									+ "][description]", new StringBody(
									entry.description,
									ContentType.TEXT_PLAIN));
						}

						// Adding multiple sequences
						httpEnt.addPart(
								"sequence" + "[" + i2 + "][encode]",
								new StringBody(entry.encode?"True":"False",
										ContentType.TEXT_PLAIN));

						if (entry.encode) {
							httpEnt.addPart(
									"sequence" + "[" + i2 + "][fps]",
									new StringBody(Float
											.toString(entry.fps),
											ContentType.TEXT_PLAIN));
						}

						if (entry.overwrite) {
							httpEnt.addPart("sequence" + "[" + i2
									+ "][overwrite]", new StringBody(
									"True", ContentType.TEXT_PLAIN));
						}
						restPost(url, httpEnt);

						i1 = 0;
						size = 0;
						httpEnt = MultipartEntityBuilder.create();
					}

					FileBody file = new FileBody(next);
					httpEnt.addPart("sequence" + "[" + i2 + "][filepath]["
							+ i1 + "]", file);

					i1++;
					size += length;
				}

				if (entry.title != null && !entry.title.isEmpty()) {
					httpEnt.addPart("sequence" + "[" + i2 + "][title]",
							new StringBody(entry.title,
									ContentType.TEXT_PLAIN));
				}

				if (entry.description != null
						&& !entry.description.isEmpty()) {
					httpEnt.addPart("sequence" + "[" + i2
							+ "][description]", new StringBody(
							entry.description, ContentType.TEXT_PLAIN));
				}

				// Adding multiple sequences
				httpEnt.addPart("sequence" + "[" + i2 + "][encode]",
						new StringBody(entry.encode?"True":"False", ContentType.TEXT_PLAIN));

				if (entry.encode) {
					httpEnt.addPart("sequence" + "[" + i2 + "][fps]",
							new StringBody(Float.toString(entry.fps),
									ContentType.TEXT_PLAIN));
				}

				if (entry.overwrite) {
					httpEnt.addPart("sequence" + "[" + i2 + "][overwrite]",
							new StringBody("True",
									ContentType.TEXT_PLAIN));
				}
				i1 = 0;
				i2++;
			}

		}

		response = restPost(url, httpEnt);
		return response;
	}

	/**
	 * Calls update_collection to update the credits
	 * 
	 * @param cid Collection ID
	 * @param credits Credits
	 * @return Response from server
	 */
	public String update_credits(int cid, String credits) {
		return update_collection(cid, null, null, false, null, null, credits,
				null, false, null, null, null, null, null, null);
	}

	/**
	 * Calls update_collection to update the description
	 * 
	 * @param cid Collection id
	 * @param description collection description
	 * @return Returns response from server
	 */
	public String update_description(int cid, String description) {

		return update_collection(cid, null, null, false, null, description,
				null, null, false, null, null, null, null, null, null);
	}

	public String update_transfer(int cid, String transfer) {

		return update_collection(cid, null, null, false, null, null,
				null, null, false, null, null, null, null, null, null);
	}

	/**
	 * Update the license
	 * 
	 * @param cid Collection ID
	 * @param license license information for SeedMe
	 * @return response from the server
	 */
	public String update_license(int cid, String license) {
		return update_collection(cid, null, null, false, null, null, null,
				license, false, null, null, null, null, null, null);
	}

	/**
	 * Update the privacy setting
	 * 
	 * @param cid collection id
	 * @param privacy Privacy settings for SeedMe
	 * @param email Email to notify
	 * @param notify notify users shared
	 * @return Response from server
	 */
	public String update_privacy(int cid, String privacy, String email,
			boolean notify) {
		return update_collection(cid, privacy, email, notify, null, null, null,
				null, false, null, null, null, null, null, null);
	}

	/**
	 * Update the title
	 * 
	 * @param cid collection id
	 * @param title collection title
	 * @return Response from server
	 */
	public String update_title(int cid, String title) {

		return update_collection(cid, null, null, false, title, null, null,
				null, false, null, null, null, null, null, null);

	}

	/**
	 * Update the privacy setting
	 * 
	 * @return service version
	 */
	public String version() {
		return this.service_version;
	}

	private String getFileType(String name) {

		if (name.endsWith(".png") || name.endsWith(".jpg")) {
			return "plot";
		} else if (name.endsWith(".txt") || name.endsWith(".pdf")
				|| name.endsWith(".xml") || name.endsWith(".dat")
				|| name.endsWith(".fig") || name.endsWith(".gz")
				|| name.endsWith(".ipynb") || name.endsWith(".kmz")
				|| name.endsWith(".kar") || name.endsWith(".m")
				|| name.endsWith(".p") || name.endsWith(".tar")
				|| name.endsWith(".tgz") || name.endsWith(".zip")
				|| name.endsWith(".kml")) {
			return "file";
		} else if (name.endsWith(".m4v") || name.endsWith(".mpg")
				|| name.endsWith(".mpeg") || name.endsWith(".mp4")
				|| name.endsWith(".webm")) {
			return "video";
		}

		return null;
	}
}
